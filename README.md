Adds syntax highlighting and filetype detection for configuration files using
the haskell [config-value](https://github.com/glguy/config-value) library.

This plugin assumes that ConFiG-Value files have a "cfgv" file extension. This
decision was made due to the assumption (for better or worse) that the "cfg"
file extension would conflict with 'something'
