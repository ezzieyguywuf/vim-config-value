" makes tpope/vim-commentary work
set commentstring=--\ %s

" Match haskell's format-comments settings
set comments=s1fl:{-,mb:-,ex:-},:--
set formatoptions+=croql
